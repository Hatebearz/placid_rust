use crate::csr_matrix::CsrMatrix;
use crate::matrix_block_structure::Structure;
use std::{
    iter::{once, Product},
    ops::{Add, Div, Sub},
};

pub struct FactorizationResult<T>
where
    T: Add + Sub + Product + Div + Default + Copy,
{
    original_matrix: Structure<T>,
    ld_matrix: CsrMatrix<T>,
}

impl<T> Structure<T>
where
    T: Add + Sub + Product + Div + Default + Copy,
{
    fn factorize_ldlt(&self) -> FactorizationResult<T> {
        let size = self.matrix.size;
        let mut rows_indexes = vec![0usize; size];
        for level in (0..self.max_level + 1).rev() {}
        unimplemented!();
        //        FactorizationResult {
        //            original_matrix: self,
        //        }
    }

    fn factorize_ldlt_symbolic(&self, rows_indexes: &mut Vec<usize>) {}
}
