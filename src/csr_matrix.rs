use permutation;
use permutation::{sort, Permutation};
use std::intrinsics::transmute;
use std::iter::Product;
use std::ops::{Add, Div, Sub};

/// Разреженная квадратная матрица в формате CSR3.
#[derive(Debug, PartialEq, Clone)]
pub struct CsrMatrix<T>
where
    T: Add + Sub + Product + Div + Default + Copy,
{
    pub values: Vec<T>,
    pub columns: Vec<usize>,
    pub rows_indexes: Vec<usize>,
    pub size: usize,
    pub is_symmetric: bool,
}

#[derive(Debug)]
pub enum InputError {
    RowsIndexesLengthInvalid,
    RowsIndexesStartsWithNonZero,
    RowsIndexesValuesDecremental(usize),
    ColumnsLengthInvalid,
    ColumnsValueGreaterThanSize(usize),
    ValuesLengthInvalid,
    SymmetricMatrixWrongFormat(usize, usize),
    ElementPositionInvalid(usize, usize),
}

impl<T> CsrMatrix<T>
where
    T: Add + Sub + Product + Div + Default + Copy,
{
    pub fn new(
        size: usize,
        values: Vec<T>,
        columns: Vec<usize>,
        rows_indexes: Vec<usize>,
        is_symmetric: bool,
    ) -> Result<CsrMatrix<T>, InputError> {
        if rows_indexes.len() != size + 1 {
            return Err(InputError::RowsIndexesStartsWithNonZero);
        }

        if *rows_indexes.first().unwrap() != 0 {
            return Err(InputError::RowsIndexesLengthInvalid);
        }

        let mut last_ci = *rows_indexes.first().unwrap();
        for (index, &ci) in rows_indexes.iter().enumerate() {
            if ci < last_ci {
                return Err(InputError::RowsIndexesValuesDecremental(index));
            }
            last_ci = ci
        }

        if columns.len() != *rows_indexes.last().unwrap() {
            return Err(InputError::ColumnsLengthInvalid);
        }

        for (index, &ci) in columns.iter().enumerate() {
            if ci >= size {
                return Err(InputError::ColumnsValueGreaterThanSize(index));
            }
        }

        if is_symmetric {
            for row in 0..size {
                for &column in &columns[rows_indexes[row]..rows_indexes[row + 1]] {
                    if column < row {
                        return Err(InputError::SymmetricMatrixWrongFormat(row, column));
                    }
                }
            }
        }

        if values.len() != *rows_indexes.last().unwrap() {
            return Err(InputError::ValuesLengthInvalid);
        }

        Ok(CsrMatrix {
            values,
            columns,
            rows_indexes,
            size,
            is_symmetric,
        })
    }

    pub fn get_diagonal_value(&self, position: usize) -> Result<&T, InputError> {
        if position > self.size {
            return Err(InputError::ElementPositionInvalid(position, position));
        }
        Ok(self
            .values
            .get(self.rows_indexes.get(position + 1).unwrap() - 1)
            .unwrap())
    }

    pub fn get_mut_diagonal_value(&mut self, position: usize) -> Result<&mut T, InputError> {
        if position > self.size {
            return Err(InputError::ElementPositionInvalid(position, position));
        }
        Ok(self
            .values
            .get_mut(self.rows_indexes.get(position + 1).unwrap() - 1)
            .unwrap())
    }

    pub fn at(&self, row: usize, column: usize) -> Result<T, String> {
        if row >= self.size {
            return Err(format!(
                "row ({}) is greater or equals matrix size ({}).",
                row, self.size
            ));
        }
        if column >= self.size {
            return Err(format!(
                "column ({}) is greater or equals matrix size ({}).",
                column, self.size
            ));
        }
        let index =
            self.columns[self.rows_indexes[row]..self.rows_indexes[row + 1]].binary_search(&column);
        Ok(match index {
            Ok(x) => self.values[x + self.rows_indexes[row]],
            Err(e) => Default::default(),
        })
    }

    fn sort_values_and_columns_in_row(
        values: &mut Vec<T>,
        columns: &mut Vec<usize>,
        rows_indexes: &Vec<usize>,
        row: usize,
    ) {
        let mut values_slice = &values[rows_indexes[row]..rows_indexes[row + 1]];
        let mut columns_slice = &columns[rows_indexes[row]..rows_indexes[row + 1]];
        let permutation = sort(columns_slice);
        let new_values = permutation.apply_slice(values_slice);
        let new_columns = permutation.apply_slice(columns_slice);
        let start_ci = rows_indexes[row];
        let non_zeroes_in_row = rows_indexes[row + 1] - start_ci;
        for i in 0..non_zeroes_in_row {
            values[start_ci + i] = new_values[i];
            columns[start_ci + i] = new_columns[i];
        }
    }

    pub fn transpose(&self) -> CsrMatrix<T> {
        let size = self.size;
        let is_symmetric = self.is_symmetric;
        let non_zeroes_count = *self.rows_indexes.last().unwrap();
        let mut values = vec![Default::default(); non_zeroes_count];
        let mut columns = vec![0 as usize; non_zeroes_count];
        let mut rows_indexes = vec![0 as usize; size + 1];
        // TODO: set structure as well.
        for i in 0..non_zeroes_count {
            rows_indexes[self.columns[i] + 1] += 1;
        }
        for i in 0..size {
            rows_indexes[i + 1] += rows_indexes[i];
        }
        let rows_indexes = rows_indexes;
        {
            let mut counters = vec![0 as usize; size];
            for row in 0..size {
                for i in self.rows_indexes[row]..self.rows_indexes[row + 1] {
                    let row_index = rows_indexes[self.columns[i]];
                    let offset = counters[self.columns[i]];
                    values[row_index + offset] = self.values[i];
                    columns[row_index + offset] = row;
                    counters[self.columns[i]] += 1;
                }
            }
        }
        for row in 1..size {
            CsrMatrix::sort_values_and_columns_in_row(
                &mut values,
                &mut columns,
                &rows_indexes,
                row,
            );
        }
        CsrMatrix {
            values,
            columns,
            rows_indexes,
            size,
            is_symmetric,
        }
    }

    pub fn permutate(&self, permutation: &Vec<usize>) -> CsrMatrix<T> {
        let size = self.size;
        let is_symmetric = self.is_symmetric;
        let non_zeroes_count = self.values.len();
        let mut values = vec![Default::default(); non_zeroes_count];
        let mut columns = vec![0usize; non_zeroes_count];
        let mut rows_indexes = vec![0usize; size + 1];
        for row in 0..size {
            let new_row = permutation[row];
            for ci in self.rows_indexes[row]..self.rows_indexes[row + 1] {
                let new_column = permutation[self.columns[ci]];
                if is_symmetric && new_column >= new_row {
                    rows_indexes[new_row + 1] += 1;
                } else {
                    rows_indexes[new_column + 1] += 1;
                }
            }
        }
        for row in 1..size {
            rows_indexes[row + 1] += rows_indexes[row];
        }
        let rows_indexes = rows_indexes;
        {
            let mut counters = vec![0 as usize; size];
            for row in 0..size {
                let new_row = permutation[row];
                for ci in self.rows_indexes[row]..self.rows_indexes[row + 1] {
                    let new_column = permutation[self.columns[ci]];
                    if is_symmetric && new_column >= new_row {
                        values[rows_indexes[new_row] + counters[new_row]] = self.values[ci];
                        columns[rows_indexes[new_row] + counters[new_row]] = new_column;
                        counters[new_row] += 1;
                    } else {
                        values[rows_indexes[new_column] + counters[new_column]] = self.values[ci];
                        columns[rows_indexes[new_column] + counters[new_column]] = new_row;
                        counters[new_column] += 1;
                    }
                }
            }
        }
        for row in 1..size {
            CsrMatrix::sort_values_and_columns_in_row(
                &mut values,
                &mut columns,
                &rows_indexes,
                row,
            );
        }
        CsrMatrix {
            values,
            columns,
            rows_indexes,
            size,
            is_symmetric,
        }
    }

    pub fn visualize(&self, filepath: &str) {
        let mut buffer = image::ImageBuffer::new(self.size as u32, self.size as u32);
        for (x, y, pixel) in buffer.enumerate_pixels_mut() {
            *pixel = image::Rgb([u8::max_value(), u8::max_value(), u8::max_value()]);
        }
        for row in 0..self.size {
            for &column in &self.columns[self.rows_indexes[row]..self.rows_indexes[row + 1]] {
                let pixel = buffer.get_pixel_mut(column as u32, row as u32);
                *pixel = image::Rgb([0, 0, 0]);
                if self.is_symmetric {
                    let pixel = buffer.get_pixel_mut(row as u32, column as u32);
                    *pixel = image::Rgb([100, 100, 100]);
                }
            }
        }
        buffer.save(filepath).unwrap();
    }
}

#[no_mangle]
pub extern "C" fn csr_matrix_new(
    values: *const [f64],
    columns: *const [usize],
    rows_indexes: *const [usize],
    size: *const usize,
) {
}

#[cfg(test)]
pub(crate) mod tests {
    extern crate test;

    use crate::csr_matrix::CsrMatrix;
    use std::fs::File;
    use std::io::{BufRead, BufReader, Read};
    use std::str::FromStr;
    use test::Bencher;

    /// 1    0    0    0    0
    /// 0    2    0    0    0
    /// 3    0    4    0    0
    /// 0    5    0    6    0
    /// 7    8    9    10   11
    fn generate_test_matrix() -> CsrMatrix<f64> {
        CsrMatrix::new(
            5 as usize,
            vec![1., 2., 3., 4., 5., 6., 7., 8., 9., 10., 11.],
            vec![0 as usize, 1, 0, 2, 1, 3, 0, 1, 2, 3, 4],
            vec![0 as usize, 1, 2, 4, 6, 11],
            false,
        )
        .unwrap()
    }

    pub fn load_test_matrix() -> CsrMatrix<f64> {
        fn read_vec_from_file<T: FromStr>(path: &str) -> Vec<T> {
            let mut s = String::new();
            let mut file = File::open(path).unwrap();
            file.read_to_string(&mut s).unwrap();
            return s
                .lines()
                .flat_map(|x| x.split_whitespace())
                .map(str::parse::<T>)
                .filter_map(Result::ok)
                .collect();
        }
        let values = read_vec_from_file::<f64>(r"matrix-example\matr.txt");
        let columns = read_vec_from_file::<usize>(r"matrix-example\idex_2.txt")
            .iter()
            .map(|x| x - 1)
            .collect();
        let rows_indexes: Vec<usize> = read_vec_from_file::<usize>(r"matrix-example\idex_1.txt")
            .iter()
            .map(|x| x - 1)
            .collect();
        let size = rows_indexes.len() - 1;
        CsrMatrix::new(size, values, columns, rows_indexes, true).unwrap()
    }

    #[test]
    fn csr_matrix_new_works() {
        load_test_matrix();
    }

    #[test]
    fn csr_matrix_get_diagonal_value_works() {
        let mut matrix = CsrMatrix::new(
            3 as usize,
            vec![1., 2., 3.],
            vec![0 as usize, 1 as usize, 2 as usize],
            vec![0 as usize, 1 as usize, 2 as usize, 3 as usize],
            true,
        )
        .unwrap();
        assert_eq!(*matrix.get_diagonal_value(1 as usize).unwrap(), 2. as f64)
    }

    #[test]
    fn csr_matrix_at_works() {
        let matrix = generate_test_matrix();
        assert_eq!(matrix.at(0, 0).unwrap(), 1.);
        assert_eq!(matrix.at(1, 0).unwrap(), 0.);
        assert_eq!(matrix.at(3, 1).unwrap(), 5.);
        assert_eq!(matrix.at(1, 3).unwrap(), 0.);
        match matrix.at(10, 2) {
            Ok(x) => panic!(),
            Err(x) => return,
        }
    }

    #[test]
    fn csr_matrix_transpose_works() {
        let matrix = load_test_matrix();
        let transposed_matrix = matrix.transpose();
        for row in 0..matrix.size {
            for column in 0..row {
                assert_eq!(matrix.at(row, column), transposed_matrix.at(column, row));
            }
        }
    }

    #[bench]
    fn csr_matrix_transpose_benchmark(b: &mut Bencher) {
        let matrix = load_test_matrix();
        b.iter(|| matrix.transpose())
    }

    #[test]
    fn csr_matrix_permutate_works() {
        let matrix = CsrMatrix::new(
            3,
            vec![1, 2, 3, 4, 5, 6, 7, 8, 9],
            vec![0, 1, 2, 0, 1, 2, 0, 1, 2],
            vec![0, 3, 6, 9],
            false,
        )
        .unwrap();
        let permutation = vec![2 as usize, 0, 1];
        let expected_result = CsrMatrix::new(
            3,
            vec![5, 6, 4, 8, 9, 7, 2, 3, 1],
            vec![0, 1, 2, 0, 1, 2, 0, 1, 2],
            vec![0, 3, 6, 9],
            false,
        )
        .unwrap();
        assert_eq!(expected_result, matrix.permutate(&permutation))
    }
}
