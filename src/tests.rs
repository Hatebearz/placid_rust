extern crate test;

use test::Bencher;
use crate::csr_matrix::CsrMatrix;
use std::{
    str::FromStr,
    iter::once,
    fs::File,
    io::Read,
};
use std::ops::{Div, Sub, Add};
use std::iter::Product;

fn calculate(i: usize) -> f64 {
    let one = i as f64 / 3.;
    let two = i as f64 / 5.;
    let three = (one + two) * two;
    three / two + one
}

fn create_vec(size: usize) -> Vec<f64> {
    let mut vec = vec![0.; size];
    for i in 0..size {
        vec[i] = calculate(i);
    }
    return vec;
}

fn append_vec(size: usize) -> Vec<f64> {
    let mut vec = Vec::<f64>::new();
    for i in 0..size {
        vec.push(calculate(i));
    }
    return vec;
}

fn func_vec(size: usize) -> Vec<f64> {
    return (0..size).map(calculate).collect();
}

#[bench]
fn create(b: &mut Bencher) {
    b.iter(|| create_vec(1000000))
}

#[bench]
fn append(b: &mut Bencher) {
    b.iter(|| append_vec(1000000))
}

#[bench]
fn func(b: &mut Bencher) {
    b.iter(|| func_vec(1000000))
}

fn old<T>(matrix: &CsrMatrix<T>) -> Vec<usize> where T: Add + Sub + Product + Div + Default + Copy  {
    let mut rows_indexes = vec![0 as usize; matrix.size + 1];
    for row in 0..matrix.size {
        rows_indexes[row + 1] = rows_indexes[row] + row - matrix.columns[matrix
            .rows_indexes[row]] + 1;
    }
    return rows_indexes;
}

fn new<T>(matrix: &CsrMatrix<T>) -> Vec<usize> where T: Add + Sub + Product + Div + Default + Copy  {
    once(0)
        .chain(
            (0..matrix.size)
                .scan(0 as usize, |state, row| {
                let cumulation = row - matrix.columns[matrix.rows_indexes[row]] + 1;
                *state += cumulation;
                Some(*state)
            })
        )
        .collect()
}

#[test]
fn equals() {
    fn read_vec_from_file<T: FromStr>(path: &str) -> Vec<T> {
        let mut s = String::new();
        let mut file = File::open(path).unwrap();
        file.read_to_string(&mut s).unwrap();
        return s.lines()
            .flat_map(|x| x.split_whitespace())
            .map(str::parse::<T>)
            .filter_map(Result::ok)
            .collect();
    }
    let values = read_vec_from_file::<f64>(
        r"D:\Rust Projects\placid_rust\matrix-example\matr.txt");
    let columns = read_vec_from_file::<usize>(
        r"D:\Rust Projects\placid_rust\matrix-example\idex_2.txt")
        .iter()
        .map(|x| x - 1)
        .collect();
    let rows_indexes: Vec<usize> = read_vec_from_file::<usize>(
        r"D:\Rust Projects\placid_rust\matrix-example\idex_1.txt")
        .iter()
        .map(|x| x - 1)
        .collect();
    let size = rows_indexes.len() - 1;
    let csr_matrix = CsrMatrix::new(size, values, columns, rows_indexes, true).unwrap();

    let old = old(&csr_matrix);
    let new = new(&csr_matrix);
    assert_eq!(old, new)
}

#[bench]
fn old_bench(b: &mut Bencher) {
    fn read_vec_from_file<T: FromStr>(path: &str) -> Vec<T> {
        let mut s = String::new();
        let mut file = File::open(path).unwrap();
        file.read_to_string(&mut s).unwrap();
        return s.lines()
            .flat_map(|x| x.split_whitespace())
            .map(str::parse::<T>)
            .filter_map(Result::ok)
            .collect();
    }
    let values = read_vec_from_file::<f64>(
        r"D:\Rust Projects\placid_rust\matrix-example\matr.txt");
    let columns = read_vec_from_file::<usize>(
        r"D:\Rust Projects\placid_rust\matrix-example\idex_2.txt")
        .iter()
        .map(|x| x - 1)
        .collect();
    let rows_indexes: Vec<usize> = read_vec_from_file::<usize>(
        r"D:\Rust Projects\placid_rust\matrix-example\idex_1.txt")
        .iter()
        .map(|x| x - 1)
        .collect();
    let size = rows_indexes.len() - 1;
    let csr_matrix = CsrMatrix::new(size, values, columns, rows_indexes, true).unwrap();

    b.iter(|| old(&csr_matrix))
}

#[bench]
fn new_bench(b: &mut Bencher) {
    fn read_vec_from_file<T: FromStr>(path: &str) -> Vec<T> {
        let mut s = String::new();
        let mut file = File::open(path).unwrap();
        file.read_to_string(&mut s).unwrap();
        return s.lines()
            .flat_map(|x| x.split_whitespace())
            .map(str::parse::<T>)
            .filter_map(Result::ok)
            .collect();
    }
    let values = read_vec_from_file::<f64>(
        r"D:\Rust Projects\placid_rust\matrix-example\matr.txt");
    let columns = read_vec_from_file::<usize>(
        r"D:\Rust Projects\placid_rust\matrix-example\idex_2.txt")
        .iter()
        .map(|x| x - 1)
        .collect();
    let rows_indexes: Vec<usize> = read_vec_from_file::<usize>(
        r"D:\Rust Projects\placid_rust\matrix-example\idex_1.txt")
        .iter()
        .map(|x| x - 1)
        .collect();
    let size = rows_indexes.len() - 1;
    let csr_matrix = CsrMatrix::new(size, values, columns, rows_indexes, true).unwrap();

    b.iter(|| new(&csr_matrix))
}