#![feature(test)]
pub mod csr_matrix;
pub mod factorization;
mod matrix_block_structure;
mod permutations;
mod tests;
