use crate::csr_matrix::CsrMatrix;
use std::{
    ops::{Add, Sub, Div},
    iter::Product
};

pub struct Permutation {
    pub vector: Vec<usize>
}

pub struct CsrMatrixRepresentation<'a, T> where T: Add + Sub + Product + Div + Default + Copy  {
    pub original_matrix: &'a CsrMatrix<T>,
    pub permutation: Permutation
}

