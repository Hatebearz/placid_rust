extern crate either;
use crate::csr_matrix::CsrMatrix;
use either::Either;
use std::cmp::max;
use std::iter::Product;
use std::mem::size_of;
use std::ops::{Add, Div, Range, Sub};

/// Чётный размер наименьшего блока должен быть на 1 больше нечётного.
const EVEN_ELEMENTARY_BLOCK_SIZE: usize = 4;
const ODD_ELEMENTARY_BLOCK_SIZE: usize = 3;

pub(crate) struct Structure<T>
where
    T: Add + Sub + Product + Div + Default + Copy,
{
    pub max_level: u32,
    pub matrix: CsrMatrix<T>,
    pub permutation: Vec<usize>,
    pub blocks: Vec<Block>,
}

impl<T> Structure<T>
where
    T: Add + Sub + Product + Div + Default + Copy,
{
    /// Создаёт структуру для матрицы.
    ///
    /// **matrix** - матрица.
    /// **width** - ширина исходной сетки.
    /// **height** - высота исходной сетки.
    fn new(matrix: &CsrMatrix<T>, width: usize, height: usize) -> Result<Structure<T>, String> {
        // Размеры сетки увеличиваются на 1 для каждой стороны.
        let width = width + 2;
        let height = height + 2;
        let size = width * height;
        if size != matrix.size {
            return Err(format!(
                "Размеры сетки ({}, {}) не совпадают с размером матрицы ({}).",
                width, height, matrix.size
            ));
        }

        // Производится расчёт количества уровней в матрице.
        let mut max_level = 0u32;
        {
            let mut width = width;
            let mut height = height;
            while width > EVEN_ELEMENTARY_BLOCK_SIZE && height > EVEN_ELEMENTARY_BLOCK_SIZE {
                max_level += 1;
                let target_variable = max(&mut width, &mut height);
                let middle_block_size = if *target_variable % 2 == 0 {
                    EVEN_ELEMENTARY_BLOCK_SIZE
                } else {
                    ODD_ELEMENTARY_BLOCK_SIZE
                };
                *target_variable = (*target_variable - middle_block_size) / 2;
            }
        }

        let blocks_count = (2usize).pow(max_level + 1) - 1;
        let mut blocks = vec![Block::new(); blocks_count];
        // Для каждого блока заполняется уровень и индекс.
        for level in 0..max_level + 1 {
            for i in 0..Structure::<T>::get_blocks_count_on_level(level) {
                let block = blocks.get_mut_block(level, i);
                block.level = level;
                block.index = i;
            }
        }
        blocks[0].width = width;
        blocks[0].height = height;
        if max_level == 0 {
            blocks[0].first_row = 0;
            blocks[0].last_row = size - 1;
            let permutated_matrix = matrix.clone();
            return Ok(Structure {
                max_level,
                matrix: permutated_matrix,
                permutation: (0..size).collect(),
                blocks,
            });
        }
        let mut permutation = vec![0usize; size];
        let mut max_index = size;
        let mut block_size = 0usize;
        // Заполняются значения для первого блока.
        {
            let block = &mut blocks[0];
            block.last_row = size - 1;
            if width <= height {
                let middle_block_height = if height % 2 == 0 {
                    EVEN_ELEMENTARY_BLOCK_SIZE
                } else {
                    ODD_ELEMENTARY_BLOCK_SIZE
                };
                block_size = middle_block_height * width;
                let child_block_height = (height - middle_block_height) / 2;
                block.min_x = 0;
                block.max_x = width - 1;
                block.min_y = child_block_height;
                block.max_y = child_block_height + middle_block_height - 1;
                Structure::<T>::fill_horizontal_block(
                    block,
                    &mut permutation,
                    width,
                    &mut max_index,
                );
            } else {
                let middle_block_width = if width % 2 == 0 {
                    EVEN_ELEMENTARY_BLOCK_SIZE
                } else {
                    ODD_ELEMENTARY_BLOCK_SIZE
                };
                block_size = middle_block_width * height;
                let child_block_width = (width - middle_block_width) / 2;
                block.min_x = child_block_width;
                block.max_x = child_block_width + middle_block_width - 1;
                block.min_y = 0;
                block.max_y = height - 1;
                Structure::<T>::fill_vertical_block(block, &mut permutation, width, &mut max_index);
            }
            block.first_row = size - block_size;
        }
        // Заполняются значения оставшихся блоков.
        let mut previous_block_first_row = blocks[0].first_row;
        let mut iterator = BlocksBackIterator::new(max_level);
        iterator.next(); // Пропускаем первый блок.
        while !iterator.is_ended {
            {
                let iterator = &iterator;
                let parent_block = blocks
                    .get_block(iterator.level - 1, iterator.level_index / 2)
                    .clone();
                let block = blocks.get_mut_block(iterator.level, iterator.level_index);
                block.last_row = previous_block_first_row - 1;
                if parent_block.width <= parent_block.height {
                    let parent_block_middle_height = if parent_block.height % 2 == 1 {
                        ODD_ELEMENTARY_BLOCK_SIZE
                    } else {
                        EVEN_ELEMENTARY_BLOCK_SIZE
                    };
                    block.height = (parent_block.height - parent_block_middle_height) / 2;
                    block.width = parent_block.width;
                    block.min_x = parent_block.min_x;
                    block.max_x = parent_block.max_x;
                    if iterator.level_index % 2 == 0 {
                        block.min_y = parent_block.min_y - block.height;
                        block.max_y = parent_block.min_y - 1;
                    } else {
                        block.min_y = parent_block.max_y + 1;
                        block.max_y = parent_block.max_y + block.height;
                    }
                } else {
                    let parent_block_middle_width = if parent_block.width % 2 == 1 {
                        ODD_ELEMENTARY_BLOCK_SIZE
                    } else {
                        EVEN_ELEMENTARY_BLOCK_SIZE
                    };
                    block.height = parent_block.height;
                    block.width = (parent_block.width - parent_block_middle_width) / 2;
                    block.min_y = parent_block.min_y;
                    block.max_y = parent_block.max_y;
                    if iterator.level_index % 2 == 0 {
                        block.min_x = parent_block.min_x - block.width;
                        block.max_x = parent_block.min_x - 1;
                    } else {
                        block.min_x = parent_block.max_x + 1;
                        block.max_x = parent_block.max_x + block.width;
                    }
                }

                let mut block_size = 0usize;
                if block.width <= block.height {
                    let middle_block_height = if block.height % 2 == 0 {
                        EVEN_ELEMENTARY_BLOCK_SIZE
                    } else {
                        ODD_ELEMENTARY_BLOCK_SIZE
                    };
                    let child_block_height = (block.height - middle_block_height) / 2;
                    if block.level != max_level {
                        block.min_y += child_block_height;
                        block.max_y -= child_block_height;
                        block_size = middle_block_height * block.width;
                        Structure::<T>::fill_horizontal_block(
                            block,
                            &mut permutation,
                            width,
                            &mut max_index,
                        );
                    } else {
                        block_size = block.width * block.height;
                        Structure::<T>::fill_vertical_block(
                            block,
                            &mut permutation,
                            width,
                            &mut max_index,
                        );
                    }
                } else {
                    let middle_block_width = if block.width % 2 == 0 {
                        EVEN_ELEMENTARY_BLOCK_SIZE
                    } else {
                        ODD_ELEMENTARY_BLOCK_SIZE
                    };
                    let child_block_width = (block.width - middle_block_width) / 2;
                    if block.level != max_level {
                        block.min_x += child_block_width;
                        block.max_x -= child_block_width;
                        block_size = middle_block_width * block.height;
                        Structure::<T>::fill_vertical_block(
                            block,
                            &mut permutation,
                            width,
                            &mut max_index,
                        );
                    } else {
                        block_size = block.width * block.height;
                        Structure::<T>::fill_horizontal_block(
                            block,
                            &mut permutation,
                            width,
                            &mut max_index,
                        );
                    }
                }
                block.first_row = block.last_row + 1 - block_size;
                previous_block_first_row = block.first_row;
            }
            iterator.next();
        }
        let permutated_matrix = matrix.permutate(&permutation);

        Ok(Structure {
            max_level,
            matrix: permutated_matrix,
            permutation,
            blocks,
        })
    }

    fn get_blocks_count_on_level(level: u32) -> usize {
        2usize.pow(level)
    }

    fn fill_horizontal_block(
        block: &Block,
        permutation: &mut Vec<usize>,
        width: usize,
        end_index: &mut usize,
    ) {
        for column in block.min_x..block.max_x + 1 {
            for row in block.min_y..block.max_y + 1 {
                *end_index -= 1;
                permutation[row * width + column] = *end_index;
            }
        }
    }
    fn fill_vertical_block(
        block: &Block,
        permutation: &mut Vec<usize>,
        width: usize,
        max_index: &mut usize,
    ) {
        for row in block.min_y..block.max_y + 1 {
            for column in block.min_x..block.max_x + 1 {
                permutation[row * width + column] = *max_index;
                *max_index -= 1;
            }
        }
    }
}

pub struct BlocksBackIterator {
    max_level: u32,
    pub level: u32,
    pub level_index: usize,
    pub is_ended: bool,
}

impl BlocksBackIterator {
    pub fn new(max_level: u32) -> BlocksBackIterator {
        BlocksBackIterator {
            max_level,
            level: 0,
            level_index: 0,
            is_ended: false,
        }
    }

    pub fn next(&mut self) {
        if self.is_ended {
            return;
        }
        if self.level == self.max_level && self.level_index == 0 {
            self.is_ended = true;
            return;
        }
        if self.level != self.max_level {
            self.level += 1;
            self.level_index = 2 * self.level_index + 1;
            return;
        }
        if self.level_index % 2 == 1 {
            self.level_index -= 1;
            return;
        }
        let levels_delta = self.level_index.trailing_zeros();
        self.level -= levels_delta;
        self.level_index = self.level_index / 2usize.pow(levels_delta) - 1;
        return;
    }
}

trait BlocksVector {
    fn get_block(&self, level: u32, index: usize) -> &Block;
    fn get_mut_block(&mut self, level: u32, index: usize) -> &mut Block;
}

impl BlocksVector for Vec<Block> {
    fn get_block(&self, level: u32, index: usize) -> &Block {
        &self[2usize.pow(level) + index - 1]
    }

    fn get_mut_block(&mut self, level: u32, index: usize) -> &mut Block {
        &mut self[2usize.pow(level) + index - 1]
    }
}

#[derive(Debug, Clone)]
pub struct Block {
    pub level: u32,
    pub index: usize,
    pub first_row: usize,
    pub last_row: usize,
    pub height: usize,
    pub width: usize,
    pub min_x: usize,
    pub min_y: usize,
    pub max_x: usize,
    pub max_y: usize,
    pub symbolic_row_views: Option<Vec<SymbolicRowView>>,
}

impl Block {
    fn new() -> Block {
        Block {
            level: 0,
            first_row: 0,
            last_row: 0,
            height: 0,
            width: 0,
            symbolic_row_views: None,
            min_x: 0,
            max_x: 0,
            min_y: 0,
            max_y: 0,
            index: 0,
        }
    }
    fn is_first(&self, max_level: u32) -> bool {
        self.level == max_level && self.index == 0
    }
}

pub type SymbolicRowView = Vec<NonZeroesSegment>;

// Отрезок строки матрицы, заполненный ненулевыми элементами.
#[derive(Debug, Clone)]
pub struct NonZeroesSegment {
    // Индекс первого элемента в отрезке.
    first_index: usize,
    // Номер колонки матрицы, в которой находится первый элемент отрезка.
    first_column: usize,
    // Номер колонки матрицы, в которой находится последний элемент отрезка.
    last_column: usize,
}

impl NonZeroesSegment {
    pub fn new(
        first_index: usize,
        first_column: usize,
        last_column: usize,
    ) -> Result<NonZeroesSegment, String> {
        if first_column > last_column {
            return Err(format!(
                "first_column ({}) больше, чем last_column ({}).",
                first_column, last_column
            ));
        }
        Ok(NonZeroesSegment {
            first_index,
            first_column,
            last_column,
        })
    }

    pub fn get_non_zeroes_count(&self) -> usize {
        self.last_column - self.first_column + 1
    }
}

#[cfg(test)]
mod tests {
    use crate::csr_matrix::tests::load_test_matrix;
    use crate::matrix_block_structure::Structure;
    use std::fs::File;
    use std::io::Write;

    #[test]
    fn structure_new_works() {
        let matrix = load_test_matrix();
        let structure = Structure::new(&matrix, 21, 21).unwrap();
    }

    #[test]
    fn test_visualize() {
        let matrix = load_test_matrix();
        matrix.visualize("matrix.png");
        let structure = Structure::new(&matrix, 21, 21).unwrap();
        let permuted_matrix = structure.matrix;
        permuted_matrix.visualize("permuted_matrix.png");
        let mut writer = File::create("permutation.txt").unwrap();
        for row in 0usize..23 {
            for column in 0usize..23 {
                write!(writer, "{} ", &structure.permutation[row * 23 + column]);
            }
            writeln!(writer);
        }
        for &i in structure.permutation.iter() {}
    }
}
